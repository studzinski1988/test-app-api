const blipp = require('blipp');
const good = require('good');

const reporters = [{
  module: 'good-squeeze',
  name: 'Squeeze',
  args: [{ /* ops: '*', */ log: '*', response: '*' }]
},
{
  module: 'good-console',
  args: [{ format: 'YYYY-MM-DD HH:mm:ss.SSS (Z)', utc: false }]
},
'stdout'];

module.exports = [
  {
    register: blipp
  },
  {
    register: good,
    options: {
      reporters: {
        consoleReporter: reporters
      }
    }
  }
];
