const Hapi = require('hapi');

const routes = require('./routes');
const plugins = require('./plugins');
const config = require('./config');

const server = new Hapi.Server({
  connections: {
    routes: {
      cors: config.cors,
      security: { xframe: { rule: 'sameorigin' } }
    },
  },
});

const printVersion = () => {
  console.info('Node.js version:', process.version);
};

const register = () => new Promise((resolve, reject) => {
  server.connection({ port: config.port });
  server.register(plugins, (err) => {
    if (err) {
      return reject(err);
    }

    server.route(routes);
    return resolve();
  });
});

Promise.resolve()
  .then(register)
  .then(printVersion)
  .then(() => server.start())
  .catch((err) => {
    console.error('API failed to start', err);
    throw err;
  });

module.exports = server;
