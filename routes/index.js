const index = require('../controllers/index');

module.exports = [
  {
    method: 'GET',
    path: '/test',
    handler: index.test,
    config: {
      tags: ['api'],
      description: 'test'
    }
  }
];
