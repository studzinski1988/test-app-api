module.exports = {
  port: process.env.API_PORT_BACKEND || 3333,
  cors: { origin: ['*'] }
};
